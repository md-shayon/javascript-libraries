const submitForm = document.getElementById("submit-form");
const editorTextarea = document.querySelector("#editor");

let editor;

ClassicEditor.create(document.querySelector("#editor"))
  .then((newEditor) => {
    editor = newEditor;
  })
  .catch((error) => {
    console.error(error);
  });

// Getting data in php
// $editor_data = $_POST[ 'content' ];
// $data = str_replace( '&', '&amp;', $data );

// js send request to server
submitForm.addEventListener("submit", (sfe) => {
  sfe.preventDefault();
  const data = editor.getData();
  const val = editor.setData(data);
  console.log({ data, value: editorTextarea.value, val });
});
