function initViz() {
  // YOUR-SERVER/views/YOUR-VISUALIZATION
  const containerDiv = document.getElementById("vizContainer"),
    MY_SERVER = "http://public.tableau.com",
    MY_VISUALIZATION = "RegionalSampleWorkbook/Storms",
    url = `${MY_SERVER}/views/${MY_VISUALIZATION}`,
    options = {
      hideTabs: true,
      onFirstInteractive: function () {
        console.log("Run this code when the viz has finished loading.");
      },
    };

  const viz = new tableau.Viz(containerDiv, url, options);
}

document.addEventListener("DOMContentLoaded", (conE) => {
  initViz();
});
