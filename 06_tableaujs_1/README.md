# Tableau
  - [Introduction to Tableau](https://www.youtube.com/watch?v=gWZtNdMko1k&list=PLWPirh4EWFpGXTBu8ldLZGJCUeTMBpJFK), [Tableau Basics For Beginners](https://www.youtube.com/watch?v=6RZEaEH9ZsQ), [Dashboard Extensions: Setting Up Your Development Environment](https://www.youtube.com/watch?v=lKdGMYKkijE)
  1. **Business intelligence**: Raw data -> business intelligence -> business insigts. A) Better businss understanding. B) Dicision making. C) Instant answer. D) Quick navigation.
  2. **Data visualization**: A) Design, B) Information Science, C) Communication. Tools: A) Tableau. B) Power BI. C) Excel
  3. **Tableau**: From connection to collaboration, Tableau is the most powerful, secure and flexible end to end analysis platform for data. A) Apt visualizations. B) Depth. C) Automatic function. D) Collection to data.
  4. **Tableau product family**: 
    A. Tableau reader - sharing with read only mode so no one can change the data (used to publish the report)
    B. Tableau public - Tableau for learners, or contibutors(used to publish the report)
    C. Tableau server - offline system based tool (A server will be setup by tableau), (used to publish the report)
    D. Tableau online - Tableau on the cloud (used to create different types of reports)
    E. Tableau desktop (used to create different types of reports)

 - [Tableau JavaScript API](https://help.tableau.com/current/api/js_api/en-us/JavaScriptAPI/js_api.htm), [Connecting to server using rest api](https://help.tableau.com/current/api/rest_api/en-us/REST/rest_api.htm), [Basic Embed](https://help.tableau.com/current/api/js_api/en-us/JavaScriptAPI/js_api_sample_basic_embed.htm), [tutorial](https://help.tableau.com/samples/en-us/js_api/tutorial.htm)
 - [Tableau JavaScript API | The most delicious ingredient for your custom applications](https://www.youtube.com/watch?v=Oda_T5PMwt0&t=946s)
 - [Embed Views into Webpages](https://help.tableau.com/current/pro/desktop/en-us/embed.htm), [Tableau embed v2 params](https://help.tableau.com/current/pro/desktop/en-us/embed_list.htm)
 - [Tableau Embedding API v3](https://help.tableau.com/current/api/embedding_api/en-us/index.html), [Tableau Embedding API Reference](https://help.tableau.com/current/api/embedding_api/en-us/reference/index.html)
 - 


https://www.youtube.com/watch?v=GuJHbHdYUnM&list=PLWPirh4EWFpGXTBu8ldLZGJCUeTMBpJFK&index=6