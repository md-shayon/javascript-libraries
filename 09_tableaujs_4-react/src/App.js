import BasicEmbed from './components/BasicEmbed';
import './App.css';

function App() {
  return (
    <div className="App">
      <h1>Basic Embed</h1>
      <BasicEmbed />
    </div>
  );
}

export default App;
