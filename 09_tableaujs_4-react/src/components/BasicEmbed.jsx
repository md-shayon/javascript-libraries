import { useRef, useEffect } from "react";
const { tableau } = window;

function BasicEmbed() {
  const basicEmbedEl = useRef(null);

  const MY_SERVER = "http://public.tableau.com";
  const MY_VISUALIZATION = "RegionalSampleWorkbook/Storms";
  const url = `${MY_SERVER}/views/${MY_VISUALIZATION}`;

  const initViz = () => {
    new tableau.Viz(basicEmbedEl.current, url, {
      width: "800px",
      height: "700px",
    });
  };

  useEffect(initViz(), []);

  return <div className="BasicEmbed" ref={basicEmbedEl} />;
}

export default BasicEmbed;
